#==============================================================================
# ■ Pokemon
# Pokemon Script Project - Krosk 
# 20/07/07
# 26/08/08 - révision, support des oeufs
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
# Restructuré et complété par Damien Linux
# 04/11/19
#-----------------------------------------------------------------------------
# Gestion individuelle
#-----------------------------------------------------------------------------

module POKEMON_S
  #------------------------------------------------------------  
  # class Pokemon : génère l'information sur un Pokémon.
  # Méthodes sur les types du Pokemon
  #------------------------------------------------------------
  class Pokemon
    def type_normal?
      return type?(1)
    end
    
    def type_fire?
      return type?(2)
    end
    alias type_feu? type_fire?
    
    def type_water?
      return type?(3)
    end
    alias type_eau? type_water?
    
    def type_electric?
      return type?(4)
    end
    
    def type_grass?
      return type?(5)
    end
    alias type_plante? type_grass?
    
    def type_ice?
      return type?(6)
    end
    alias type_glace? type_ice?
    
    def type_fighting?
      return type?(7)
    end
    alias type_combat? type_fighting?
    
    def type_poison?
      return type?(8)
    end
    
    def type_ground?
      return type?(9)
    end
    alias type_sol? type_ground?
    
    def type_fly?
      return type?(10)
    end
    alias type_vol? type_fly?
    
    def type_psy?
      return type?(11)
    end
    
    def type_insect?
      return type?(12)
    end
    
    def type_rock?
      return type?(13)
    end
    alias type_roche? type_rock?
    
    def type_ghost?
      return type?(14)
    end
    alias type_spectre? type_ghost?
    
    def type_dragon?
      return type?(15)
    end
    
    def type_steel?
      return type?(16)
    end
    alias type_acier? type_steel?
    
    def type_dark?
      return type?(17)
    end
    alias type_tenebres? type_dark?
    
    # Renvoie true si le Pokémon est de type Fée sinon false
    def type_fee?
      return type?(18)
    end
    
    def type?(number)
      return [type1, type2].include?(number)
    end
    alias type_custom? type? 
  end
end