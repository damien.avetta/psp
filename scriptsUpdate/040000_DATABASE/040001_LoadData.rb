#==============================================================================
# ● Base de données - Chargement des données
# Pokemon Script Project - Krosk 
# Load_Data - Damien Linux
# 14/11/2020
#==============================================================================
module POKEMON_S
  class Load_Data
    class << self
      def load_in_thread
        $time_save = Time.now
        # Chargement des Pokémon existant
        Load_Pokemon.load
        # Chargement des compétences des Pokémon existantes
        Load_Skill.load
        # Chargement des groupes
        Load_Group.load
      end

      def load_component_with_dependencies_data
        # On charge les éléments nécessitant les éléments précédent
        # Chargement des objets
        Load_Items.load
        # Chargement des liens groupes - zones
        Load_Group.load_infos_zone
      end
      
      def no_hanging_script
        last_time = Time.now
        time_wait = last_time - $time_save
        if time_wait > 5
          Graphics.update
          $time_save = last_time
        end
      end
    end
  end
end