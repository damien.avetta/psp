dépendance : aucune

Dans TrainerPartyActor :
l.80 :
Remplacer :
pokemon.id_ball = @ball if @ball
      pokemon.set_skills(@moves) if @moves
      pokemon.stats_modifier(@stats) if @stats 
      pokemon.dv_modifier(@iv) if @iv
      pokemon.ev_modifier(@ev) if @ev
      pokemon.ability=(@ability) if @ability
      pokemon.gender=(@genre) if @genre
      pokemon.form = @form if @form
Par :
pokemon.form = @form if @form # Note : à laisser avant "@moves" (sinon @moves écrasés par alt_moverpool)
      pokemon.id_ball = @ball if @ball
      pokemon.set_skills(@moves) if @moves
      pokemon.stats_modifier(@stats) if @stats 
      pokemon.dv_modifier(@iv) if @iv
      pokemon.ev_modifier(@ev) if @ev
      pokemon.ability=(@ability) if @ability
      pokemon.gender=(@genre) if @genre
	  
Dans PokemonBattleTrainer :
l.66 :
Remplacer :
Trainer_Info.pokemon_list(@trainer_id).each do |pokemon_info|
        $battle_var.enemy_party.actors.push(pokemon_info.create_pokemon)
      end
Par :
Trainer_Info.pokemon_list(@trainer_id).each do |pokemon_info|
        pokemon = pokemon_info.create_pokemon 
        $battle_var.enemy_party.actors.push(pokemon) 
        pokemon.alt_movepool(true) if pokemon_info.moves == nil or pokemon_info.moves == [] # Perso movepool formes d'Alola (autre dans Interpreter)
      end