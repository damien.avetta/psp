dépendance : aucune

Dans Attaques :
Définir 515:Cotogarde Cible sur : "7 - L'utilisateur" au lieu de "1 - Un ennemi"
Définir 309:POINT METEOR la puissance et la précision à 90 au lieu de respectivement 100 et 85
Définir 555:TUNNELIER la "0/Ph 1/Sp 2/St" à 0 au lieu de 2
Définir 499:DRACO-QUEUE l''ID Effet à 28 au lieu de 0
Définir 282:SABOTAGE la puissance à 65 au lieu de 20

Dans Evolutions 
Pour la ligne 595 : décocher la CT72-AVALANCHE